package com.frank.weather.utilities;

public class ConversionCalculation {
	public static int temperatureCalculationFromFahrenheit(Double fahrenheit){
		return Math.round(5 / 9 * (Math.round(fahrenheit) - 32));
	}
	
	public static int temperatureCalculationFromCelcius(Double celcius){
		return Math.round(9 * Math.round(celcius) / 5 + 32);
	}
	
	public static long precipitationCalculation(Double precipIntensity){
		return Math.round(precipIntensity * 100d);
	}
}
