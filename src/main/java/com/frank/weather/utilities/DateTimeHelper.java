package com.frank.weather.utilities;

import java.time.Instant;
import java.time.ZoneId;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

public class DateTimeHelper {
	public static String toStringWithFormat(Instant date, String format) {
		DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(format)
	            .withZone(ZoneId.of("UTC"));
		
		return DATE_TIME_FORMATTER.format(date);
	}
}
