package com.frank.weather.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.frank.weather.model.Country;

public interface CountryRepository extends MongoRepository<Country, String> {
	public Country findByCountryId(String countryId);
	public List<Country> findAll();
}
