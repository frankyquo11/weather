package com.frank.weather.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.frank.weather.model.Country;
import com.frank.weather.model.DailyWeather;

public interface DailyWatherRepository extends MongoRepository<DailyWeather, String>{
	List<DailyWeather> findByCountry(Country country);
}
