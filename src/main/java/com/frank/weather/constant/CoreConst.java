package com.frank.weather.constant;

public class CoreConst {
	public static final String YEAR_TO_SECOND = "yyyy-MM-dd HH:mm:ss";
	public static final String YEAR_TO_DAY = "yyyy-MM-dd";
	public static final String HOUR = "HH";
}
