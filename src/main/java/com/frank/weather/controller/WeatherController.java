package com.frank.weather.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import tk.plogitech.darksky.forecast.ForecastException;

import com.frank.weather.core.WeatherCore;

@Controller
@RequestMapping("/getWeather")
public class WeatherController {

	@Autowired
    private WeatherCore weatherCore;
	
	@GetMapping
    public ModelAndView getWeather(){
		ModelAndView mav = new ModelAndView("weather");
		try {
			mav.addObject("datas", weatherCore.GetCurrentWeather());
		} catch (ForecastException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
        return mav;
    }
}
