package com.frank.weather.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import tk.plogitech.darksky.forecast.ForecastException;

import com.frank.weather.core.WeatherCore;

@Component
public class CronJob {
	@Autowired
    private WeatherCore weatherCore;

	@Scheduled(cron="0 0 0 * * ?")
    public void WeatherCronJobService(){
		System.out.println("Start Daily Weather Request");
		try {
			weatherCore.getDailyWeather();
		} catch (ForecastException e) {
			e.printStackTrace();
		}
		System.out.println("End Daily Weather Request");
    }

	@Scheduled(cron="0 0 1 * * ?")
    public void ArchieveCronJobService(){
		System.out.println("Start Archieve Request");
		//Do the archieve Jobs.
//		try {
//			weatherCore.getDailyWeather();
//		} catch (ForecastException e) {
//			e.printStackTrace();
//		}
		System.out.println("End Archieve Request");
    }
}
