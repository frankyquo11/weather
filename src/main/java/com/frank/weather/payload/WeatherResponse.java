package com.frank.weather.payload;

public class WeatherResponse {
	private String countryId;
	private String countryName;
	private String currentTime;
	private String currentSummary;
	private String currentIcon;
	private long currentPrecipitation;
	private int currentTemperatureCelcius;
	private int currentTemperatureFahrenheit;
	private int currentPressure;
	
	public WeatherResponse() {
		super();
	}

	public WeatherResponse(String countryId, String countryName,
			String currentTime, String currentSummary, String currentIcon,
			long currentPrecipitation, int currentTemperatureCelcius,
			int currentTemperatureFahrenheit, int currentPressure) {
		super();
		this.countryId = countryId;
		this.countryName = countryName;
		this.currentTime = currentTime;
		this.currentSummary = currentSummary;
		this.currentIcon = currentIcon;
		this.currentPrecipitation = currentPrecipitation;
		this.currentTemperatureCelcius = currentTemperatureCelcius;
		this.currentTemperatureFahrenheit = currentTemperatureFahrenheit;
		this.currentPressure = currentPressure;
	}

	public String getCountryId() {
		return countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getCurrentTime() {
		return currentTime;
	}

	public void setCurrentTime(String currentTime) {
		this.currentTime = currentTime;
	}

	public String getCurrentSummary() {
		return currentSummary;
	}

	public void setCurrentSummary(String currentSummary) {
		this.currentSummary = currentSummary;
	}

	public String getCurrentIcon() {
		return currentIcon;
	}

	public void setCurrentIcon(String currentIcon) {
		this.currentIcon = currentIcon;
	}

	public long getCurrentPrecipitation() {
		return currentPrecipitation;
	}

	public void setCurrentPrecipitation(long currentPrecipitation) {
		this.currentPrecipitation = currentPrecipitation;
	}

	public int getCurrentTemperatureCelcius() {
		return currentTemperatureCelcius;
	}

	public void setCurrentTemperatureCelcius(int currentTemperatureCelcius) {
		this.currentTemperatureCelcius = currentTemperatureCelcius;
	}

	public int getCurrentTemperatureFahrenheit() {
		return currentTemperatureFahrenheit;
	}

	public void setCurrentTemperatureFahrenheit(int currentTemperatureFahrenheit) {
		this.currentTemperatureFahrenheit = currentTemperatureFahrenheit;
	}

	public int getCurrentPressure() {
		return currentPressure;
	}

	public void setCurrentPressure(int currentPressure) {
		this.currentPressure = currentPressure;
	}
}
