package com.frank.weather.model;

import java.io.Serializable;
import java.util.UUID;

import org.springframework.data.annotation.Id;

public class DailyWeather{
	@Id
	private UUID id;
	
	private Country country;
	private String date;
	private String summary;
	private String icon;
	private Double precipitation;
	private String precipitationType;
	private Double precipitationAccumulation;
	private Double precipitationMax;
	private String precipitationMaxTime;
	private int apparentTemperatureHighCelcius;
	private String apparentTemperatureHighTimeCelcius;
	private int apparentTemperatureLowCelcius;
	private String apparentTemperatureLowTimeCelcius;
	private int apparentTemperatureMaxCelcius;
	private String apparentTemperatureMaxTimeCelcius;
	private int apparentTemperatureMinCelcius;
	private String apparentTemperatureMinTimeCelcius;
	private int temperatureHighCelcius;
	private String temperatureHighTimeCelcius;
	private int temperatureLowCelcius;
	private String temperatureLowTimeCelcius;
	private int temperatureMaxCelcius;
	private String temperatureMaxTimeCelcius;
	private int temperatureMinCelcius;
	private String temperatureMinTimeCelcius;
	private int apparentTemperatureHighFahrenheit;
	private int apparentTemperatureLowFahrenheit;
	private int apparentTemperatureMaxFahrenheit;
	private int apparentTemperatureMinFahrenheit;
	private int temperatureHighFahrenheit;
	private int temperatureLowFahrenheit;
	private int temperatureMaxFahrenheit;
	private int temperatureMinFahrenheit;
	private int pressure;

	public DailyWeather() {
		super();
	}

	public DailyWeather(Country country, String date,
			String summary, String icon, Double precipitation,
			String precipitationType, Double precipitationAccumulation,
			Double precipitationMax, String precipitationMaxTime,
			int apparentTemperatureHighCelcius,
			String apparentTemperatureHighTimeCelcius,
			int apparentTemperatureLowCelcius,
			String apparentTemperatureLowTimeCelcius,
			int apparentTemperatureMaxCelcius,
			String apparentTemperatureMaxTimeCelcius,
			int apparentTemperatureMinCelcius,
			String apparentTemperatureMinTimeCelcius,
			int temperatureHighCelcius, String temperatureHighTimeCelcius,
			int temperatureLowCelcius, String temperatureLowTimeCelcius,
			int temperatureMaxCelcius, String temperatureMaxTimeCelcius,
			int temperatureMinCelcius, String temperatureMinTimeCelcius,
			int apparentTemperatureHighFahrenheit,
			int apparentTemperatureLowFahrenheit,
			int apparentTemperatureMaxFahrenheit,
			int apparentTemperatureMinFahrenheit,
			int temperatureHighFahrenheit, int temperatureLowFahrenheit,
			int temperatureMaxFahrenheit, int temperatureMinFahrenheit,
			int pressure) {
		super();
		this.country = country;
		this.date = date;
		this.summary = summary;
		this.icon = icon;
		this.precipitation = precipitation;
		this.precipitationType = precipitationType;
		this.precipitationAccumulation = precipitationAccumulation;
		this.precipitationMax = precipitationMax;
		this.precipitationMaxTime = precipitationMaxTime;
		this.apparentTemperatureHighCelcius = apparentTemperatureHighCelcius;
		this.apparentTemperatureHighTimeCelcius = apparentTemperatureHighTimeCelcius;
		this.apparentTemperatureLowCelcius = apparentTemperatureLowCelcius;
		this.apparentTemperatureLowTimeCelcius = apparentTemperatureLowTimeCelcius;
		this.apparentTemperatureMaxCelcius = apparentTemperatureMaxCelcius;
		this.apparentTemperatureMaxTimeCelcius = apparentTemperatureMaxTimeCelcius;
		this.apparentTemperatureMinCelcius = apparentTemperatureMinCelcius;
		this.apparentTemperatureMinTimeCelcius = apparentTemperatureMinTimeCelcius;
		this.temperatureHighCelcius = temperatureHighCelcius;
		this.temperatureHighTimeCelcius = temperatureHighTimeCelcius;
		this.temperatureLowCelcius = temperatureLowCelcius;
		this.temperatureLowTimeCelcius = temperatureLowTimeCelcius;
		this.temperatureMaxCelcius = temperatureMaxCelcius;
		this.temperatureMaxTimeCelcius = temperatureMaxTimeCelcius;
		this.temperatureMinCelcius = temperatureMinCelcius;
		this.temperatureMinTimeCelcius = temperatureMinTimeCelcius;
		this.apparentTemperatureHighFahrenheit = apparentTemperatureHighFahrenheit;
		this.apparentTemperatureLowFahrenheit = apparentTemperatureLowFahrenheit;
		this.apparentTemperatureMaxFahrenheit = apparentTemperatureMaxFahrenheit;
		this.apparentTemperatureMinFahrenheit = apparentTemperatureMinFahrenheit;
		this.temperatureHighFahrenheit = temperatureHighFahrenheit;
		this.temperatureLowFahrenheit = temperatureLowFahrenheit;
		this.temperatureMaxFahrenheit = temperatureMaxFahrenheit;
		this.temperatureMinFahrenheit = temperatureMinFahrenheit;
		this.pressure = pressure;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public Double getPrecipitation() {
		return precipitation;
	}

	public void setPrecipitation(Double precipitation) {
		this.precipitation = precipitation;
	}

	public String getPrecipitationType() {
		return precipitationType;
	}

	public void setPrecipitationType(String precipitationType) {
		this.precipitationType = precipitationType;
	}

	public Double getPrecipitationAccumulation() {
		return precipitationAccumulation;
	}

	public void setPrecipitationAccumulation(Double precipitationAccumulation) {
		this.precipitationAccumulation = precipitationAccumulation;
	}

	public Double getPrecipitationMax() {
		return precipitationMax;
	}

	public void setPrecipitationMax(Double precipitationMax) {
		this.precipitationMax = precipitationMax;
	}

	public String getPrecipitationMaxTime() {
		return precipitationMaxTime;
	}

	public void setPrecipitationMaxTime(String precipitationMaxTime) {
		this.precipitationMaxTime = precipitationMaxTime;
	}

	public int getApparentTemperatureHighCelcius() {
		return apparentTemperatureHighCelcius;
	}

	public void setApparentTemperatureHighCelcius(int apparentTemperatureHighCelcius) {
		this.apparentTemperatureHighCelcius = apparentTemperatureHighCelcius;
	}

	public String getApparentTemperatureHighTimeCelcius() {
		return apparentTemperatureHighTimeCelcius;
	}

	public void setApparentTemperatureHighTimeCelcius(
			String apparentTemperatureHighTimeCelcius) {
		this.apparentTemperatureHighTimeCelcius = apparentTemperatureHighTimeCelcius;
	}

	public int getApparentTemperatureLowCelcius() {
		return apparentTemperatureLowCelcius;
	}

	public void setApparentTemperatureLowCelcius(int apparentTemperatureLowCelcius) {
		this.apparentTemperatureLowCelcius = apparentTemperatureLowCelcius;
	}

	public String getApparentTemperatureLowTimeCelcius() {
		return apparentTemperatureLowTimeCelcius;
	}

	public void setApparentTemperatureLowTimeCelcius(
			String apparentTemperatureLowTimeCelcius) {
		this.apparentTemperatureLowTimeCelcius = apparentTemperatureLowTimeCelcius;
	}

	public int getApparentTemperatureMaxCelcius() {
		return apparentTemperatureMaxCelcius;
	}

	public void setApparentTemperatureMaxCelcius(int apparentTemperatureMaxCelcius) {
		this.apparentTemperatureMaxCelcius = apparentTemperatureMaxCelcius;
	}

	public String getApparentTemperatureMaxTimeCelcius() {
		return apparentTemperatureMaxTimeCelcius;
	}

	public void setApparentTemperatureMaxTimeCelcius(
			String apparentTemperatureMaxTimeCelcius) {
		this.apparentTemperatureMaxTimeCelcius = apparentTemperatureMaxTimeCelcius;
	}

	public int getApparentTemperatureMinCelcius() {
		return apparentTemperatureMinCelcius;
	}

	public void setApparentTemperatureMinCelcius(int apparentTemperatureMinCelcius) {
		this.apparentTemperatureMinCelcius = apparentTemperatureMinCelcius;
	}

	public String getApparentTemperatureMinTimeCelcius() {
		return apparentTemperatureMinTimeCelcius;
	}

	public void setApparentTemperatureMinTimeCelcius(
			String apparentTemperatureMinTimeCelcius) {
		this.apparentTemperatureMinTimeCelcius = apparentTemperatureMinTimeCelcius;
	}

	public int getTemperatureHighCelcius() {
		return temperatureHighCelcius;
	}

	public void setTemperatureHighCelcius(int temperatureHighCelcius) {
		this.temperatureHighCelcius = temperatureHighCelcius;
	}

	public String getTemperatureHighTimeCelcius() {
		return temperatureHighTimeCelcius;
	}

	public void setTemperatureHighTimeCelcius(String temperatureHighTimeCelcius) {
		this.temperatureHighTimeCelcius = temperatureHighTimeCelcius;
	}

	public int getTemperatureLowCelcius() {
		return temperatureLowCelcius;
	}

	public void setTemperatureLowCelcius(int temperatureLowCelcius) {
		this.temperatureLowCelcius = temperatureLowCelcius;
	}

	public String getTemperatureLowTimeCelcius() {
		return temperatureLowTimeCelcius;
	}

	public void setTemperatureLowTimeCelcius(String temperatureLowTimeCelcius) {
		this.temperatureLowTimeCelcius = temperatureLowTimeCelcius;
	}

	public int getTemperatureMaxCelcius() {
		return temperatureMaxCelcius;
	}

	public void setTemperatureMaxCelcius(int temperatureMaxCelcius) {
		this.temperatureMaxCelcius = temperatureMaxCelcius;
	}

	public String getTemperatureMaxTimeCelcius() {
		return temperatureMaxTimeCelcius;
	}

	public void setTemperatureMaxTimeCelcius(String temperatureMaxTimeCelcius) {
		this.temperatureMaxTimeCelcius = temperatureMaxTimeCelcius;
	}

	public int getTemperatureMinCelcius() {
		return temperatureMinCelcius;
	}

	public void setTemperatureMinCelcius(int temperatureMinCelcius) {
		this.temperatureMinCelcius = temperatureMinCelcius;
	}

	public String getTemperatureMinTimeCelcius() {
		return temperatureMinTimeCelcius;
	}

	public void setTemperatureMinTimeCelcius(String temperatureMinTimeCelcius) {
		this.temperatureMinTimeCelcius = temperatureMinTimeCelcius;
	}

	public int getApparentTemperatureHighFahrenheit() {
		return apparentTemperatureHighFahrenheit;
	}

	public void setApparentTemperatureHighFahrenheit(
			int apparentTemperatureHighFahrenheit) {
		this.apparentTemperatureHighFahrenheit = apparentTemperatureHighFahrenheit;
	}

	public int getApparentTemperatureLowFahrenheit() {
		return apparentTemperatureLowFahrenheit;
	}

	public void setApparentTemperatureLowFahrenheit(
			int apparentTemperatureLowFahrenheit) {
		this.apparentTemperatureLowFahrenheit = apparentTemperatureLowFahrenheit;
	}

	public int getApparentTemperatureMaxFahrenheit() {
		return apparentTemperatureMaxFahrenheit;
	}

	public void setApparentTemperatureMaxFahrenheit(
			int apparentTemperatureMaxFahrenheit) {
		this.apparentTemperatureMaxFahrenheit = apparentTemperatureMaxFahrenheit;
	}

	public int getApparentTemperatureMinFahrenheit() {
		return apparentTemperatureMinFahrenheit;
	}

	public void setApparentTemperatureMinFahrenheit(
			int apparentTemperatureMinFahrenheit) {
		this.apparentTemperatureMinFahrenheit = apparentTemperatureMinFahrenheit;
	}

	public int getTemperatureHighFahrenheit() {
		return temperatureHighFahrenheit;
	}

	public void setTemperatureHighFahrenheit(int temperatureHighFahrenheit) {
		this.temperatureHighFahrenheit = temperatureHighFahrenheit;
	}

	public int getTemperatureLowFahrenheit() {
		return temperatureLowFahrenheit;
	}

	public void setTemperatureLowFahrenheit(int temperatureLowFahrenheit) {
		this.temperatureLowFahrenheit = temperatureLowFahrenheit;
	}

	public int getTemperatureMaxFahrenheit() {
		return temperatureMaxFahrenheit;
	}

	public void setTemperatureMaxFahrenheit(int temperatureMaxFahrenheit) {
		this.temperatureMaxFahrenheit = temperatureMaxFahrenheit;
	}

	public int getTemperatureMinFahrenheit() {
		return temperatureMinFahrenheit;
	}

	public void setTemperatureMinFahrenheit(int temperatureMinFahrenheit) {
		this.temperatureMinFahrenheit = temperatureMinFahrenheit;
	}

	public int getPressure() {
		return pressure;
	}

	public void setPressure(int pressure) {
		this.pressure = pressure;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}
}
