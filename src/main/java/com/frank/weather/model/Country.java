package com.frank.weather.model;

import org.springframework.data.annotation.Id;

public class Country {
	@Id
	private String countryId;
	
	private String countryName;
	private double latitude;
	private double longitude;
	
	public Country() {
		super();
	}
	public Country(String countryId, String countryName, double latitude,
			double longitude) {
		super();
		this.countryId = countryId;
		this.countryName = countryName;
		this.latitude = latitude;
		this.longitude = longitude;
	}
	
	public String getCountryId() {
		return countryId;
	}
	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
}
