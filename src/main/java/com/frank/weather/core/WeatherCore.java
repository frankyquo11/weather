package com.frank.weather.core;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import com.frank.weather.constant.DarkSkyConst;
import com.frank.weather.constant.CoreConst;
import com.frank.weather.model.Country;
import com.frank.weather.model.DailyWeather;
import com.frank.weather.payload.WeatherResponse;
import com.frank.weather.repository.CountryRepository;
import com.frank.weather.repository.DailyWatherRepository;
import com.frank.weather.utilities.ConversionCalculation;
import com.frank.weather.utilities.DateTimeHelper;
import com.frank.weather.utilities.IntegerHelper;

import tk.plogitech.darksky.api.jackson.DarkSkyJacksonClient;
import tk.plogitech.darksky.forecast.APIKey;
import tk.plogitech.darksky.forecast.ForecastException;
import tk.plogitech.darksky.forecast.ForecastRequest;
import tk.plogitech.darksky.forecast.ForecastRequestBuilder;
import tk.plogitech.darksky.forecast.GeoCoordinates;
import tk.plogitech.darksky.forecast.model.DailyDataPoint;
import tk.plogitech.darksky.forecast.model.Forecast;
import tk.plogitech.darksky.forecast.model.HourlyDataPoint;
import tk.plogitech.darksky.forecast.model.Latitude;
import tk.plogitech.darksky.forecast.model.Longitude;

@Service
public class WeatherCore {
	
	@Autowired
	private CountryRepository countryRepository;
	
	@Autowired
	private DailyWatherRepository dailyWeatherRepository;
	
	public List<WeatherResponse> GetCurrentWeather() throws ForecastException {
		List<WeatherResponse> response = new ArrayList<WeatherResponse>();
		
		List<Country> countries = countryRepository.findAll();
		for(Country country : countries) {
			ForecastRequest request = new ForecastRequestBuilder()
		        .key(new APIKey(DarkSkyConst.SECRET_KEY))
		        .language(ForecastRequestBuilder.Language.en)
		        .exclude(ForecastRequestBuilder.Block.minutely)
		        .exclude(ForecastRequestBuilder.Block.hourly)
		        .exclude(ForecastRequestBuilder.Block.daily)
		        .location(new GeoCoordinates(new Longitude(country.getLongitude()), new Latitude(country.getLatitude()))).build();
		
		    DarkSkyJacksonClient client = new DarkSkyJacksonClient();
		    Forecast forecast = client.forecast(request);
		    
		    response.add(new WeatherResponse(country.getCountryId(), country.getCountryName(), 
		    		DateTimeHelper.toStringWithFormat(forecast.getCurrently().getTime(), CoreConst.YEAR_TO_SECOND), 
		    		forecast.getCurrently().getSummary(), forecast.getCurrently().getIcon(), 
		    		ConversionCalculation.precipitationCalculation(forecast.getCurrently().getPrecipIntensity()), 
		    		IntegerHelper.toInterger(forecast.getCurrently().getTemperature()), 
		    		ConversionCalculation.temperatureCalculationFromCelcius(forecast.getCurrently().getTemperature()), 
		    		IntegerHelper.toInterger(forecast.getCurrently().getPressure())));
		}
		return response;
	}
	
	public void getDailyWeather() throws ForecastException {
    	List<DailyWeather> dailyWeathers = new ArrayList<DailyWeather>();
    	
    	List<Country> countries = countryRepository.findAll();
		for(Country country : countries) {
			ForecastRequest request = new ForecastRequestBuilder()
	        .key(new APIKey(DarkSkyConst.SECRET_KEY))
	        .language(ForecastRequestBuilder.Language.en)
	        .exclude(ForecastRequestBuilder.Block.minutely)
	        .exclude(ForecastRequestBuilder.Block.currently)
	        .location(new GeoCoordinates(new Longitude(country.getLongitude()), new Latitude(country.getLatitude()))).build();
			
			DarkSkyJacksonClient client = new DarkSkyJacksonClient();
		    Forecast forecast = client.forecast(request);
		    
		    List<DailyWeather> dws = dailyWeatherRepository.findByCountry(country);
		    HashMap<String, DailyWeather> hdws = new HashMap<String, DailyWeather>();
		    for(DailyWeather dw : dws) {
		    	hdws.put(dw.getDate(), dw);
		    }
		    
		    for(DailyDataPoint ddp : forecast.getDaily().getData()) {
		    	DailyWeather dailyWeather = null;
		    	String date =  DateTimeHelper.toStringWithFormat(ddp.getTime(), CoreConst.YEAR_TO_SECOND);
		    	
		    	if(dws == null)
		    	{
		    		continue;
		    	}
		    	else if(hdws.containsKey(date)){
		    		dailyWeather = hdws.get(date);
		    	}
		    	else {
		    		dailyWeather = new DailyWeather(country, date,
		    			ddp.getSummary(), ddp.getIcon(), ddp.getPrecipIntensity(), ddp.getPrecipType(), 
		    			ddp.getPrecipAccumulation(), ddp.getPrecipIntensityMax(),
		    			DateTimeHelper.toStringWithFormat(ddp.getPrecipIntensityMaxTime(), CoreConst.YEAR_TO_SECOND), 
		    			IntegerHelper.toInterger(ddp.getApparentTemperatureHigh()),
		    			DateTimeHelper.toStringWithFormat(ddp.getApparentTemperatureHighTime(), CoreConst.YEAR_TO_SECOND), 
		    			IntegerHelper.toInterger(ddp.getApparentTemperatureLow()),
		    			DateTimeHelper.toStringWithFormat(ddp.getApparentTemperatureLowTime(), CoreConst.YEAR_TO_SECOND), 
		    			IntegerHelper.toInterger(ddp.getApparentTemperatureMax()),
		    			DateTimeHelper.toStringWithFormat(ddp.getApparentTemperatureMaxTime(), CoreConst.YEAR_TO_SECOND), 
		    			IntegerHelper.toInterger(ddp.getApparentTemperatureMin()),
		    			DateTimeHelper.toStringWithFormat(ddp.getApparentTemperatureMinTime(), CoreConst.YEAR_TO_SECOND), 
		    			IntegerHelper.toInterger(ddp.getTemperatureHigh()),
		    			DateTimeHelper.toStringWithFormat(ddp.getTemperatureHighTime(), CoreConst.YEAR_TO_SECOND), 
		    			IntegerHelper.toInterger(ddp.getTemperatureLow()),
		    			DateTimeHelper.toStringWithFormat(ddp.getTemperatureLowTime(), CoreConst.YEAR_TO_SECOND), 
		    			IntegerHelper.toInterger(ddp.getTemperatureMax()),
		    			DateTimeHelper.toStringWithFormat(ddp.getTemperatureMaxTime(), CoreConst.YEAR_TO_SECOND), 
		    			IntegerHelper.toInterger(ddp.getTemperatureMin()),
		    			DateTimeHelper.toStringWithFormat(ddp.getTemperatureMinTime(), CoreConst.YEAR_TO_SECOND), 
		    			ConversionCalculation.temperatureCalculationFromCelcius(ddp.getApparentTemperatureHigh()),
		    			ConversionCalculation.temperatureCalculationFromCelcius(ddp.getApparentTemperatureLow()),
		    			ConversionCalculation.temperatureCalculationFromCelcius(ddp.getApparentTemperatureMax()),
		    			ConversionCalculation.temperatureCalculationFromCelcius(ddp.getApparentTemperatureMin()),
		    			ConversionCalculation.temperatureCalculationFromCelcius(ddp.getTemperatureHigh()),
		    			ConversionCalculation.temperatureCalculationFromCelcius(ddp.getTemperatureLow()),
		    			ConversionCalculation.temperatureCalculationFromCelcius(ddp.getTemperatureMax()),
		    			ConversionCalculation.temperatureCalculationFromCelcius(ddp.getTemperatureMin()),
		    			IntegerHelper.toInterger(ddp.getPressure()));
		    		dailyWeather.setId(UUID.randomUUID());
		    	}
		    	
		    	dailyWeathers.add(dailyWeather);
		    }
		    if(dailyWeathers != null) {
		    	dailyWeatherRepository.saveAll(dailyWeathers);
		    }
		}
	}
}
