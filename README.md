# Getting Started

[Download](https://bitbucket.org/frankyquo11/weather)
or clone from
[GIT](https://bitbucket.org/frankyquo11/weather) and then
use Maven and Java 


## How To Build The Project

    $ cd weather
    $ mvn clean install
    $ mvn spring-boot:run


## High level design

The main component is on Core package, it will be used to call from everywhere when the function is similiar.
There's 2 models: 1 is persistence layer, 1 is payload layer. It will more handy if more features near futures
I haven't used any design pattern yet, because it just a simple apps for calling weather endpoint.


## What I build

* The project mainly already done in getting currently weather forecast, while i want to add more feature where i click the country will be display detail view, with the daily forecast.
* Daily cron job will be running daily, to get daily data to MongoDB. (planning to get hourly too)
* Archieve module not implemented yet, but will be running daily to sweeping all outdated date.


## Bootstrap

I'm using Fuse template for the design.


## Token

MongoDB & DarkSky token already provided in the code, no need further setup.

